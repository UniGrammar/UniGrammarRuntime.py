import typing
from abc import abstractmethod
from pathlib import Path

from UniGrammarRuntimeCore.IParser import IParserFactory as IParserFactoryCore, IParserFactoryFromPrecompiled as IParserFactoryFromPrecompiledCore, IParserFactoryFromSource as IParserFactoryFromSourceCore, IParser

# pylint:disable=too-few-public-methods


class IParserFactory(IParserFactoryCore):
	__slots__ = ()

	@abstractmethod
	def fromBundle(self, grammarResources: "InMemoryGrammarResources"):
		"""Creates an executor from the files within bundle"""
		raise NotImplementedError


class IParserFactoryFromSource(IParserFactoryFromSourceCore):  # pylint:disable=abstract-method
	__slots__ = ()

	def fromBundle(self, grammarResources: "InMemoryGrammarResources") -> IParser:
		return self.fromInternal(self.getSource(grammarResources))  # since they cannot be precompiled, for them internal repr is source text

	def getSource(self, grammarResources: "InMemoryGrammarResources") -> str:
		"""Must return source code of the grammar in its DSL"""
		return grammarResources.parent.backendsTextData[self.__class__.PARSER_CLASS.NAME, grammarResources.name + "." + (self.__class__.EXT if self.__class__.EXT is not None else self.__class__.PARSER_CLASS.NAME)]


class IParserFactoryFromPrecompiled(IParserFactoryFromPrecompiledCore):  # pylint:disable=abstract-method
	__slots__ = ()

	def fromBundle(self, grammarResources: "InMemoryGrammarResources") -> IParser:
		ctor = self.compile(self.getSource(grammarResources), grammarResources.name)
		return self.fromInternal(ctor())

	def getSource(self, grammarResources: "InMemoryGrammarResources") -> "ast.Module":
		"""Must return source code of the grammar in its DSL"""
		return grammarResources.parent.backendsPythonAST[self.__class__.PARSER_CLASS.NAME, grammarResources.name]


class IParserFactoryFromPrecompiledOrSource(IParserFactoryFromSourceCore):
	"""Hybrid between `IParserFromPrecompiled` and `IParserFromSource`:
		tries to find and use precompiled file first,
		if there is no, tries to find and use source
	"""

	PRECOMPILED = None
	SOURCE = None

	__slots__ = ("_precompiled", "_source")

	def __init__(self):
		self._precompiled = None
		self._source = None
		super().__init__()

	@property
	def precompiled(self) -> IParserFactoryFromPrecompiled:
		res = self._precompiled
		if res is None:
			self._precompiled = res = self.__class__.PRECOMPILED()
		return res

	@property
	def source(self) -> IParserFactoryFromSource:
		res = self._source
		if res is None:
			self._source = res = self.__class__.SOURCE()
		return res

	def fromBundle(self, grammarResources: "InMemoryGrammarResources"):
		"""tries to find and use precompiled file first,
		if there is no, tries to find and use source"""
		try:
			return self.precompiled.fromBundle(grammarResources)
		except FileNotFoundError:
			return self.source.fromBundle(grammarResources)

	def compileStr(self, grammarText: str, target: typing.Any = None, fileName: typing.Optional[typing.Union[Path, str]] = None):
		"""Proxies to the factory defined by `SOURCE`"""
		return self.source.compileStr(grammarText, target, fileName)

	def compileFile(self, grammarFile: Path, target: typing.Any = None):
		"""Proxies to the factory defined by `SOURCE`"""
		return self.source.compileFile(grammarFile, target)
